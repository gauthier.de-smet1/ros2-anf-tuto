```shell
sudo locale-gen en_US en_US.UTF-8
sudo apt update && sudo apt install locales
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8
sudo apt install software-properties-common curl gnupg lsb-release
sudo add-apt-repository universe
sudo apt update

```

##  Add Official ROS 2 Humble repository to Ubuntu

```shell
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(source /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
```

## Update the ROS 2 package index

```shell
sudo apt update
sudo apt upgrade
```

## Install ROS 2 Humble on Ubuntu 22.04 and the development tools (compiler, ...)

```shell
sudo apt install ros-humble-desktop
sudo apt install ros-dev-tools
```

## Set up ROS 2 Humble environment
```shell
echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

```shell
echo "source /opt/ros/humble/setup.zsh" >> ~/.zshrc
source ~/.zshrc
```

## Verify ROS 2 installation

We will launch a talker (coded in C++) and a listener (coded in Python) in 2 different terminals.

```shell
cd /opt/ros/humble/lib/demo_nodes_cpp/

ros2 run demo_nodes_cpp talker
```
```shell
cd /opt/ros/humble/lib/demo_nodes_cpp/

ros2 run demo_nodes_py listener
```

You must get messages in the 2 terminals ouput showing the transmission of messages through ROS2 topics. 
